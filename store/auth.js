export const state = () => ({
    token: '',
    userData: ''
});

export const mutations = {
    setToken(state, data) {
        state.token = data;
    },
    setUserData(state, data) {
        state.userData = data;
    }
};

export const getters = {
    isAuth(state) {
        return state.token !== '';
    }
};
