export const state = () => ({
    visibility: false
});

export const mutations = {
    show(state) {
        state.visibility = true;
    },
    hide(state) {
        state.visibility = false;
    }
};
