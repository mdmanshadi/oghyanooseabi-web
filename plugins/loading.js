export default ({ app, store }, inject) => {
    inject('loadingIndicator', {
        show () {
            store.commit('loading/show');
        },
        hide(){
            store.commit('loading/hide');
        }
    })
}
